package yoni.com.movies

import android.arch.lifecycle.MutableLiveData
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.mockk.every
import io.mockk.mockk
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.standalone.inject
import org.koin.test.KoinTest
import org.mockito.MockitoAnnotations
import yoni.com.models.db.DbService
import yoni.com.models.db.DbServiceImpl
import yoni.com.models.movies.MoviesService
import yoni.com.models.movies.MoviesServiceImpl
import yoni.com.models.network.NetworkService
import yoni.com.models.network.NetworkServiceImpl
import yoni.com.movies.viewmodels.MainViewModel

class MainViewModelUnitTest : KoinTest {

    val mainViewModel: MainViewModel by inject()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)

        val appModule = module {
            single<NetworkService> { NetworkServiceImpl() }
            single<DbService> { DbServiceImpl() }
            single<MoviesService> { MoviesServiceImpl(get(), get()) }
            viewModel { MainViewModel(get()) }
        }

        startKoin(arrayListOf(appModule))
    }


    @Test
    fun `should do it`() {

        val screenState = mockk<MutableLiveData<MainViewModel.ScreenState>>()



        every { screenState.value } returns MainViewModel.ScreenState.POPULATED
//        every {
//            screenState.value
//        } returns


        mainViewModel.textChanged("brrr")


    }
}