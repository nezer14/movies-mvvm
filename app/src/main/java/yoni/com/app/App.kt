package yoni.com.app

import android.app.Application
import io.reactivex.plugins.RxJavaPlugins
import org.koin.android.ext.android.startKoin
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import yoni.com.models.db.DbService
import yoni.com.models.db.DbServiceImpl
import yoni.com.models.movies.MoviesService
import yoni.com.models.movies.MoviesServiceImpl
import yoni.com.models.network.NetworkService
import yoni.com.models.network.NetworkServiceImpl
import yoni.com.movies.viewmodels.MainViewModel

class App : Application() {

    companion object {
        lateinit var instance: App
    }

    val appModule = module {
        single<NetworkService> { NetworkServiceImpl() }
        single<DbService> { DbServiceImpl() }
        single<MoviesService> { MoviesServiceImpl(get(), get()) }
        viewModel { MainViewModel(get()) }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        startKoin(this, listOf(appModule))
        RxJavaPlugins.setErrorHandler { _ -> }
    }

}