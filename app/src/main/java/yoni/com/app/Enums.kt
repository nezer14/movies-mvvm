package yoni.com.app

import yoni.com.movies.R
import java.util.concurrent.TimeUnit

object Enums {
    enum class Debounce(val time: Long, val unit: TimeUnit) {
        BOTTOM(100, TimeUnit.MILLISECONDS)
    }

    enum class ScreenState(
        val shouldSpin: Boolean,
        val hasError: Boolean? = null,
        val errorReasonTitle: String? = null,
        val errorReasonBody: String? = null
    ) {
        EMPTY(false, true, App.instance.getString(R.string.start_typing), App.instance.getString(R.string.empty)),
        ERROR(false, true, App.instance.getString(R.string.problem), App.instance.getString(R.string.check_internet)),
        NO_RESULTS(false, true, App.instance.getString(R.string.no_results), App.instance.getString(R.string.query_no_results)),
        POPULATED(false, false),
        LOADING(true)
    }
}