import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

fun RecyclerView.addReachToBottomListener(onBottomReached: () -> Unit) {
    val lm = this.layoutManager as LinearLayoutManager
    this.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (dy > 10) {
                if (lm.findLastVisibleItemPosition() == lm.itemCount - 1) {
                    onBottomReached.invoke()
                }
            }
            super.onScrolled(recyclerView, dx, dy)
        }
    })
}

fun RecyclerView.pageScroll(it: Int) {
    if (it > 1) {
        this.smoothScrollBy(0, 200)
    }
}

