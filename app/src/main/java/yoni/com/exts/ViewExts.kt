import android.annotation.SuppressLint
import android.graphics.Color
import android.net.Uri
import android.text.Spannable
import android.text.SpannableString
import android.text.style.BackgroundColorSpan
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import yoni.com.adapters.SearchTextWatcher
import yoni.com.app.App
import yoni.com.movies.R

fun View.shouldBeVisible(shouldSpin: Boolean?) {
    shouldSpin?.let {
        if (it) {
            this.visibility = View.VISIBLE
        } else {
            this.visibility = View.INVISIBLE
        }
    }
}

fun EditText.onTextChanged(textChanged: (String) -> Unit) {
    this.addTextChangedListener(object : SearchTextWatcher({
        textChanged.invoke(it)
    }) {})

}

fun TextView.applyMarker(queryText: String) {
    val tvt = this.text.toString()
    var ofe = tvt.indexOf(queryText, 0)
    val wordToSpan = SpannableString(this.text)
    var ofs = 0
    while (ofs < tvt.length && ofe != -1) {
        ofe = tvt.indexOf(queryText, ofs)
        if (ofe == -1)
            break
        else {
            wordToSpan.setSpan(
                BackgroundColorSpan(Color.parseColor("#ff6666")),
                ofe,
                ofe + queryText.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            this.setText(wordToSpan, TextView.BufferType.SPANNABLE)
        }
        ofs = ofe + 1
    }
}

fun ImageView.withUrl(url: String?) {
    val resources = App.instance.resources
    val requestOptions = getErrorHandlingRO()
    val parse = Uri.parse(resources.getString(R.string.img_base_endpoint))
    val fixedUri = parse
        .buildUpon()
        .appendPath(url?.removePrefix("/"))
        .build()

    val imageRequest = Glide.with(context).setDefaultRequestOptions(requestOptions).load(fixedUri)
    imageRequest.into(this)
}


fun ImageView.setImageUrl(path: String?) {
    val requestOptions = getErrorHandlingRO()
    App.instance.let {
        Glide.with(it)
            .setDefaultRequestOptions(requestOptions)
            .load(App.instance.getString(R.string.img_base_endpoint) + path)
            .into(this)
    }

}

@SuppressLint("CheckResult")
private fun getErrorHandlingRO(): RequestOptions {
    val requestOptions = RequestOptions()
    requestOptions.placeholder(R.drawable.movie_placeholder)
    requestOptions.error(R.drawable.error)
    return requestOptions
}





