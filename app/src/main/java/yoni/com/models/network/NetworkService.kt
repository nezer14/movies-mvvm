package yoni.com.models.network

import io.reactivex.Observable
import yoni.com.datamodels.MovieSearchResponse

interface NetworkService {

    fun findMovieByText(query: String, page: Int): Observable<MovieSearchResponse>?
}