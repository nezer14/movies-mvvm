package yoni.com.models.network

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import yoni.com.app.App
import yoni.com.datamodels.MovieSearchResponse
import yoni.com.movies.R

class NetworkServiceImpl : NetworkService {

    private val networkApiService by lazy {
        NetworkApi.create()
    }

    override fun findMovieByText(query: String, page: Int): Observable<MovieSearchResponse>? {
        return networkApiService
            .getMovie(page, query, App.instance.getString(R.string.tmdb_token))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}