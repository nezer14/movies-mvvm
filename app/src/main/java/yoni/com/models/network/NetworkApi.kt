package yoni.com.models.network

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import yoni.com.app.App
import yoni.com.datamodels.MovieSearchResponse
import yoni.com.movies.R

interface NetworkApi {

    @GET("search/movie")
    fun getMovie(
        @Query("page") page: Int,
        @Query("query") searchText: String,
        @Query("api_key") token: String

    ): Observable<MovieSearchResponse>

    companion object {
        fun create(): NetworkApi {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(App.instance.getString(R.string.movies_base_endpoint))
                .build()

            return retrofit.create(NetworkApi::class.java)
        }
    }
}