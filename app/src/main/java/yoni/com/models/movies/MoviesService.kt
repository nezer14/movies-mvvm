package yoni.com.models.movies

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import yoni.com.datamodels.Movie

interface MoviesService {
    fun search(query: String): Observable<List<Movie>>?
    fun page(query: String): Observable<List<Movie>>?
    val pagingIndicator: Observable<Int>?
    val cachedMovie: BehaviorSubject<Movie>
}