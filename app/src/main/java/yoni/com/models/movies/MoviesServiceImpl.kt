package yoni.com.models.movies

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import yoni.com.datamodels.Movie
import yoni.com.models.db.DbService
import yoni.com.models.network.NetworkService

class MoviesServiceImpl(private val networkService: NetworkService, val dbService: DbService) :
    MoviesService {

    override val cachedMovie: BehaviorSubject<Movie> = BehaviorSubject.create()
    override val pagingIndicator = PublishSubject.create<Int>()
    private var page = 1

    override fun search(query: String): Observable<List<Movie>>? {
        page = 1
        return findMovie(query)?.doOnNext { pagingIndicator.onNext(1) }
    }

    private fun findMovie(query: String): Observable<List<Movie>>? {
        return networkService
            .findMovieByText(query, page)
            ?.map { movieSearchResponse ->
                movieSearchResponse.movies
            }
    }

    override fun page(query: String): Observable<List<Movie>>? {
        return findMovie(query)
            ?.doOnNext { listOfMovies ->
                if (listOfMovies.isNotEmpty()) {
                    pagingIndicator.onNext(++page)
                }
            }
    }
}