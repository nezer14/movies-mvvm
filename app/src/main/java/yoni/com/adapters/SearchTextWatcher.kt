package yoni.com.adapters

import android.text.Editable
import android.text.TextWatcher

open class SearchTextWatcher(private val onUserInput: (activate: String) -> Unit) : TextWatcher {
    override fun afterTextChanged(editable: Editable?) {
        editable?.let {
            onUserInput.invoke(it.toString())
        }
    }

    override fun beforeTextChanged(cs: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(cs: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

}