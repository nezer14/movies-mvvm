package yoni.com.adapters

import android.widget.ImageView
import android.widget.TextView
import applyMarker
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import setImageUrl
import yoni.com.datamodels.Movie
import yoni.com.movies.R


class MoviesAdapter(dataset: List<Movie>? = null) :
    BaseQuickAdapter<Movie, BaseViewHolder>(R.layout.layout_animation, dataset) {

    private var queryText = ""

    override fun convert(helper: BaseViewHolder, item: Movie) {
        helper.setText(R.id.title, item.title)
        helper.setText(R.id.description, item.overview)
        helper.setText(R.id.date, item.releaseDate)
        (helper.getView(R.id.poster) as ImageView).setImageUrl(item.posterPath)
        (helper.getView(R.id.description) as TextView).applyMarker(queryText)
        helper.addOnClickListener(R.id.container)
    }

    fun update(queryText: String, it: List<Movie>) {
        this.queryText = queryText
        setNewData(it)
    }
}

