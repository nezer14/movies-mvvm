package yoni.com.movies.viewmodels

import android.app.ActivityOptions
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.support.v4.app.FragmentActivity
import android.view.View
import io.reactivex.subjects.PublishSubject
import yoni.com.app.Enums
import yoni.com.datamodels.Movie
import yoni.com.models.movies.MoviesService
import yoni.com.movies.views.SecondaryActivity


class MainViewModel(private var moviesService: MoviesService) : ViewModel() {

    private val buttomReachedObservable = PublishSubject.create<String>()
    private val movies: MutableLiveData<List<Movie>> by lazy { MutableLiveData<List<Movie>>() }
    private val pagingIndicator: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }
    private val states: MutableLiveData<Enums.ScreenState> by lazy { MutableLiveData<Enums.ScreenState>() }


    init {
        val pageChanged = moviesService
            .pagingIndicator
            ?.distinctUntilChanged()
            ?.subscribe {
                pagingIndicator.value = it
            }

        val bottomReached = buttomReachedObservable
            .debounce(Enums.Debounce.BOTTOM.time, Enums.Debounce.BOTTOM.unit)
            .subscribe {
                moviesService
                    .page(it)
                    ?.filter { pagedList ->
                        pagedList.isNotEmpty()
                    }?.map { pagedList ->
                        val currValue = movies.value as ArrayList<Movie>
                        currValue.addAll(pagedList)
                        currValue
                    }?.subscribe { arr ->
                        movies.value = arr

                    }
            }
    }

    fun textChanged(newText: String) {
        if (newText.isEmpty()) {
            states.value = Enums.ScreenState.EMPTY
            movies.value = null
        } else {
            states.value = Enums.ScreenState.LOADING
            val searchTextChanged = moviesService
                .search(newText)
                ?.subscribe({ listOfMovies ->
                    if (listOfMovies.isNotEmpty()) {
                        states.value = Enums.ScreenState.POPULATED
                    } else {
                        states.value = Enums.ScreenState.NO_RESULTS
                    }
                    movies.value = listOfMovies
                }, { _ ->
                    states.value = Enums.ScreenState.ERROR
                })
        }
    }

    fun movieClicked(movie: Movie, view: View) {
        moviesService.cachedMovie.onNext(movie)
        view.context?.let { activity ->
            if (activity is FragmentActivity) {
                val intent = SecondaryActivity.newIntent(activity)
                activity.startActivity(
                    intent,
                    ActivityOptions
                        .makeSceneTransitionAnimation(
                            activity,
                            view,
                            "fadein"
                        )
                        .toBundle()
                )
            }
        }
    }

    fun bottomReached(queryText: String) {
        buttomReachedObservable.onNext(queryText)
    }
    fun movieChanges() = movies
    fun pagingIndicatorChanges() = pagingIndicator
    fun stateChanges() = states
}