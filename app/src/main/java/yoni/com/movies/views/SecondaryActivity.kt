package yoni.com.movies.views

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_secondary.*
import org.koin.android.ext.android.inject
import withUrl
import yoni.com.models.movies.MoviesService
import yoni.com.movies.R

class SecondaryActivity : AppCompatActivity() {

    val moviesService: MoviesService by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_secondary)
        setSupportActionBar(toolbar)
        val cachedMovie = moviesService.cachedMovie.value
        cachedMovie?.let {
            poster.withUrl(it.posterPath)
            overview.text = it.overview
            rating.numStars = it.voteAverage.toInt()
            titleTv.text = it.title
            toolbar_title.title = it.title
            release.text = it.releaseDate
        }
    }

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, SecondaryActivity::class.java)
        }
    }
}
