package yoni.com.movies.views

import addReachToBottomListener
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.chad.library.adapter.base.BaseQuickAdapter
import kotlinx.android.synthetic.main.activity_main.*
import onTextChanged
import org.koin.android.viewmodel.ext.android.viewModel
import pageScroll
import shouldBeVisible
import yoni.com.adapters.MoviesAdapter
import yoni.com.app.Enums
import yoni.com.datamodels.Movie
import yoni.com.movies.R
import yoni.com.movies.viewmodels.MainViewModel


class MainActivity : AppCompatActivity() {

    private val mainViewModel by viewModel<MainViewModel>()
    private var moviesAdapter: MoviesAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initTextWatcher()
        observeMovieChanges()
        observeScreenStateChanges()
        observePagingIndicator()
        initAdapter()
    }

    private fun observeMovieChanges() {
        mainViewModel
            .movieChanges()
            .observe(this, Observer { movieList ->
                movieList?.let {
                    moviesAdapter?.update(searchEt.text.toString(), it)
                }
            })
    }

    private fun observeScreenStateChanges() {
        mainViewModel
            .stateChanges()
            .observe(this, Observer { screenState ->
                screenState?.let { it ->
                    updateViews(it)
                }
            })
    }

    private fun observePagingIndicator() {
        mainViewModel
            .pagingIndicatorChanges()
            .observe(this, Observer { isPaging ->
                isPaging?.let {
                    pagerIndicator.animateText("Page $it")
                    recyclerview.pageScroll(it)
                }
            })
    }

    private fun updateViews(it: Enums.ScreenState) {
        spinner.shouldBeVisible(it.shouldSpin)
        recyclerview.shouldBeVisible(it.hasError?.not())
        indicativeContainer.shouldBeVisible(it.hasError)
        textTitle.text = it.errorReasonTitle
        textReason.text = it.errorReasonBody
    }


    private fun initTextWatcher() {
        searchEt.onTextChanged {
            mainViewModel.textChanged(it)
        }
    }

    private fun initAdapter() {
        recyclerview.setHasFixedSize(true)
        recyclerview.layoutManager = LinearLayoutManager(this)
        moviesAdapter = MoviesAdapter()
        moviesAdapter?.onItemChildClickListener = BaseQuickAdapter.OnItemChildClickListener { adapter, view, position ->
            mainViewModel.movieClicked(adapter.data[position] as Movie, view)
        }
        moviesAdapter?.openLoadAnimation()
        registerToBottomReachEvent()
        recyclerview.adapter = moviesAdapter
    }

    private fun registerToBottomReachEvent() {
        recyclerview.addReachToBottomListener {
            mainViewModel
                .bottomReached(searchEt.text.toString())
        }
    }
}


